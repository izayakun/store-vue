import Vue from 'vue'
import Router from 'vue-router'
import Signin from '@/components/Signin.vue'
import Signup from '@/components/Signup.vue'
import Pets from '@/components/Pets/Pets.vue'
import PetShow from '@/components/Pets/PetShow.vue'
import PetCreate from '@/components/Pets/PetCreate.vue'
import PetEdit from '@/components/Pets/PetEdit.vue'
import Users from '@/components/Users/Users.vue'
import UserShow from '@/components/Users/UserShow.vue'
import UserCreate from '@/components/Users/UserCreate.vue'
import UserEdit from '@/components/Users/UserEdit.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'pets-list',
      component: Pets
    },
    {
      path: '/pets/:id',
      name: 'pet-show',
      component: PetShow,
      props: true
    },
    {
      path: '/pet/create',
      name: 'pet-create',
      component: PetCreate
    },
    {
      path: '/pets/:id',
      name: 'pet-edit',
      component: PetEdit,
      props: true
    },
    {
      path: '/users',
      name: 'users-list',
      component: Users
    },
    {
      path: '/users/:id',
      name: 'user-show',
      component: UserShow,
      props: true
    },
    {
      path: '/user/create',
      name: 'user-create',
      component: UserCreate
    },
    {
      path: '/users/:id',
      name: 'user-edit',
      component: UserEdit
    },
    {
      path: '/signin',
      name: 'signin',
      component: Signin
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup
    }
  ]
})
